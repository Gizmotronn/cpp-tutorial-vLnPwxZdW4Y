# About this repo
I will be learning C, C++ & C# on this repo using a number of different tutorials, including Sololearn. This is for me to be able to code some stuff with Stellarios & Unity

# Contents
* [ACORD Repo](http://acord.tech/github)
* [ACORD Gitlab](http://gitlab.com/acord-robotics)
* [My Gitlab](http://acord.tech/gitlab)
* [Packages](#packages)
* [Planned software](#games-and-apps-i-plan-to-make)
* [Tutorials](#tutorials)
    * [Sololearn](#sololearn)
        * [Stuff from Sololearn](#stuff-from-sololearn)
    * [Codecademy](#codecademy)
    * [Other Tutorials](#other-tutorials)
    * [Youtube](#youtube)
    * [Ray Wenderlich](https://www.raywenderlich.com/603984-beginning-programming-with-c)

# Packages
* [Unity 2D Game Kit](https://assetstore.unity.com/packages/essentials/tutorial-projects/2d-game-kit-107098)


# Games and Apps I plan to make
* Stellarios Droid Controller
* Star Sailors Game

# Tutorials
## Sololearn
* [Learn Code @ Sololearn](http://sololearn.com)
* [My Profile](https://www.sololearn.com/Profile/5134550)

### Stuff from Sololearn
* [PDF on coding books](https://www.sololearn.com/Discuss/1849121/guys-if-you-want-free-pdf-of-programming)
* [Youtube Channels](https://www.sololearn.com/Discuss/1844910/good-youtube-channels)

## Codecademy
* [Codecademy Site](http://codecademy.com)

## Unity
* [2D Game Dev](https://learn.unity.com/tutorial/2d-game-kit-walkthrough?courseId=5c5c1e08edbc2a5465c7ec01&projectId=5c51481eedbc2a00206944f0)

## Other tutorials
### Reviews
* [Hongkiat](https://www.hongkiat.com/blog/sites-to-learn-coding-online/)
* [Forbes](https://www.forbes.com/sites/laurencebradford/2016/12/08/11-websites-to-learn-to-code-for-free-in-2017/)
* [Thenextweb](https://thenextweb.com/dd/2017/04/03/so-you-want-to-be-a-programmer-huh-heres-25-ways-to-learn-online/)

### Online tutorial sites
* [Khanacademy](https://www.khanacademy.org/computing/computer-programming)
* [Hackerrank](https://www.hackerrank.com/)
* [EDX](https://www.edx.org/)
* [Upskill](https://upskillcourses.com/)
* [The Odin Project](https://www.theodinproject.com/) - no C#/C++
* [Freecodecamp](https://www.freecodecamp.org/)
* [Coursera](https://www.coursera.org/courses?query=coding&)

#### Paid
* [Pluralsight](https://www.pluralsight.com/browse/software-development/c-sharp)
* [Treehouse $$$](https://teamtreehouse.com/)
* [Codeavengers](https://www.codeavengers.com/jr)

#### Competitive
* [Codewars](https://www.codewars.com/)

I think due to video tutorials being more hands-on, the primary tutorials I follow to start with will be from the Unity website and Youtube. We will be learning C# & C++ with Unity exclusively for now.

## C++ Tutorial - Youtube
### Youtube

https://www.youtube.com/watch?v=vLnPwxZdW4Y

<iframe width="560" height="315" src="https://www.youtube.com/embed/vLnPwxZdW4Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>