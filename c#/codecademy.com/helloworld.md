# What is C#?
	• If you can name it, you can probably build it with C#
	• The programming language can be used to make:
		○ Interactive websites
		○ Mobile apps
		○ Video games
		○ Augmented Reality (AR)
		○ Virtual Reality (VR)
		○ Desktop applications
		○ Back-end services
	• Pokemon Go, Stack Overflow website were built with frameworks that can be run with C# (Unity, ASP.NET)

C# can make you a better programmer:


	• C# has you define the type of each data in a program
		○ Assigning a data type tells a computer what operations can and cannot be performed on a piece of data
		○ This style helps programmers avoid a lot of errors that are found on languages like Java and Ruby
	• C# programs are built by defining objects that interact with each other
		○ This makes the code reusable and easy to manage
		
		
# Run some C#
	• The line Console.Writeline("Hello World"); prints text to a console
		○ Whatever is in the middle of the parenthesis (brackets) is printed in the console
        * In this case, Hello World is printed

```c#
Console.Writeline("Hello World");
```

The full code for Console.Writeline is 
```c#
using System;

namespace HelloWorld
{
  class Program
  {
    static void Main()
    {
      Console.WriteLine("Go to acord.tech/opus");    
     }
  }
}
```

	• The console can also read/take input from the user
		○ The command that is used to do this is "Console.Readline()
        * It captures text that a user types into the console
        
The full code for the input function: 
```c#
using System;

namespace GettingInput
{
  class Program
  {
    static void Main()
    {
      Console.WriteLine("Who is your favourite droid??");
      string input = Console.ReadLine();
      Console.WriteLine($"I think {input} is a pretty cool droid");
    }
  }
}
```

• The word input represents a variable
		○ It represents the text the user typed in the console
		○ It's labelled a string
		○ A piece of text is called a string in C#
• dotnet run 	Is used to run commands from interactive code

# Comments
* In C#, anything written after a // or between a /* and */ is a comment
* The computer (console) ignores comments
* Comments can:
    * Provide context for why something is written the way it is
    * Help other people to understand the code
    * Ignore a line of code and see how the program will run without it

Finished
End