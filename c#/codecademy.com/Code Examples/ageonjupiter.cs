using System;

namespace PlanetCalculations
{
  class Program
  {
    static void Main(string[] args)
    {  
      // Your Age
      // Your age on different planets depends on the orbital period of the planet you are on.
			int userAge = 16;

      // Length of years on Jupiter (in Earth years)
			double jupiterYears = 11.86;

      // Age on Jupiter
			double jupiterAge = userAge/jupiterYears;

      // Time to Jupiter
			double journeyToJupiter = 6.142466;

      // New Age on Earth
			double newEarthAge = userAge + journeyToJupiter;

      // New Age on Jupiter
			double newJupiterAge = newEarthAge/jupiterYears;

      // Log calculations to console
			Console.WriteLine(newJupiterAge);
      Console.WriteLine(newEarthAge);
      Console.WriteLine(userAge);
      Console.WriteLine(newEarthAge);
      Console.WriteLine(jupiterAge);


    }
  }
}
