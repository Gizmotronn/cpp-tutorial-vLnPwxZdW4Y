// I should probably copy these to their own folder in "code examples"

// I'm going to try to see what each of these functions does and annotate it here

using System;

namespace MadTeaParty // the name of the string?
{
  class Program
  {
    static void Main(string[] args)
    { 
        // this means that whenever drink is mentioned in the code (as in {drink}, it is referring to wine
      string drink = "wine";
      string madTeaParty = $"\"Have some {drink},\" the March Hare said in an encouraging tone. // Line break \nAlice looked all round the table, but there was nothing on it but tea.\n\"I don't see any {drink},\" she remarked.\n\"There isn't any,\" said the March Hare.";

      int storyLength = madTeaParty.Length; // how many words are in the "madTeaParty
      string toFind = "March Hare";

      string findLowerCase = toFind.ToLower(); 
      int findMarchHare = madTeaParty.IndexOf(toFind);

      Console.WriteLine(madTeaParty.Substring(findMarchHare));
      Console.WriteLine($"This scene is {storyLength} long.\n");
      Console.WriteLine($"The term we're looking for is {toFind} and is located at index {findMarchHare}.");

    }
  }
}
