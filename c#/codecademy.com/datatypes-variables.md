# Introduction to Data Types & Variables in C#
	• When writing programs, the programmer is telling the computer how to process pieces of information
		○ For example, numbers in a calculation, or printing text in the console
	• Languages like C# tell a computer about the type of data in a program using data types
		○ Data types represent the different types of information that we use in our programs, and how they should be used
	• C# is strongly typed
		○ It requires us to specify the data types that we're using
	• It is also statistically typed 
		○ This means that it will check that we used the correct types before the program runs
	• C# Data types include:
		○ Int (4567) - whole integer number
		○ String (kangaroo) - a piece of text
		○ Bool (true) - represents the logical idea of true or false

# C# Data Types 
	• Data types tell us a few things about a piece of data:
		○ How it can be stored
		○ What operations we can perform with it
		○ Different methods it can be used with
	• Data types are present in all programming languages
		○ However, they are particularly important in C# & C++
		○ That's because C# is a strongly-typed language
		○ It requires the programmer to specify the data type of every value and expression
	• The most important data types:
		○ Int  (whole numbers - 5)
		○ Double (decimal numbers - 3.1415)
		○ Char (single character - a, b)
		○ String (String of "char" - abc)
        * Bool (Boolean values, like "true or false")

# Creating variables with types
	• When we use data in our programs, it is good to save them in a variable
		○ A variable is like a box in our computer memory where we store values used in our code
	• In C#, data types and variables are closely intertwined
		○ Every time we declare a variable, we have to define its data type (the data type it is going to hold)
	• We can define a variable on 2 lines:

```c#
// declare an integer
int myAge;
myAge = 32;
```

	• Or we can do it on one:
```c#
// declare a string
string countryName = "Netherlands";
```

	• In both cases, we first write the data type, then the variable name, then an equals sign is used to assign a variable a value
	• We can then use the variables throughout our program:

```c#
int evenNumber = 22;
int oddNumber = 45;
Console.WriteLine(evenNumber + oddNumber); // Prints 67
Console.WriteLine(oddNumber - evenNumber); // Prints 23
```

Defining variables in a small program:

```c#
using System;

namespace Form
{
  class Program
  {
    static void Main(string[] args)
    {
      // Create Variables
      string name = "Shadow"; 
      string breed = "Golden Retriever";
      int age = 5;
      double weight = 65.22;
      bool spayed = true;

      // Print variables to the console
      Console.WriteLine(name);
      Console.WriteLine(breed);
      Console.WriteLine(age);
      Console.WriteLine(weight);
      Console.WriteLine(spayed);


    }
  }
}
```

Notes:
* No brackets for int variable

# Handling Errors

	• When you program, you'll come across a lot of errors
	• What happens when you forget to specify a data type for a variable?

As in 

```c#
randomData = "asdf jskdf";
Console.WriteLine(randomData);
```

	• C# gives an error
		○ It doesn't want you to have random data being used in your program
		○ The error will look like this:

```c#
The name 'randomData' does not exist in the current context [CS0103:] 
```

	• In you use the wrong data type, you'll get this error:

```c#
Cannot implicitly convert type 'double' to 'int'. An explicit conversion exists (are you missing a cast?)
```

	• There are some words you can't use - reserved keywords:
		○ Words that the language uses
		○ We can't name a variable "string", for example
	• End each statement with a semicolon (;)

# Converting Data Types
	• Data type conversion - when the data type of a variable is changed
	• Doubles can't be turned into integers, but integers can be turned into doubles:

```c#
int myInt = 3;
// Turn it into a decimal
double MyDouble = myInt;
```
	• There are a couple of different ways to do data conversion:
		○ Implicit Conversion: Happens automatically if no data will be lost in the conversion
		○ Explicit Conversion: Requires a cast operator to convert a data type into another one (doubles into integers, for example:

```c#
double myDouble = 3.2;

// Round myDouble to the nearest whole number
int myInt = (int)myDouble;
```

__Test about Converting Data Types__
```c#
using System;

namespace FavoriteNumber
{
  class Program
  {
    static void Main(string[] args)
    {
      // Ask user for fave number
      Console.Write("Enter your favorite number!: ");

      // Turn that answer into an int
      int faveNumber = 
      Console.ReadLine();


    }
  }
}
```

I figured out how to fix part 1, as originally it line 159 and 160 were on the same line, but it looked in the editor that they were on different lines. Placing an enter between line 159 & 160 fixed the error!

By doing this, I earnt Max Streak 2 Badge!

## Numerical Data types
Pizza shop/chain example: done (see https://gitlab.com/IrisDroidology/cpp-tutorial-vLnPwxZdW4Y/blob/master/c%23/codecademy.com/Code%20Examples/giant-brutus-pizza.cs)

## Arithmetic Operators
```c#
int answer = 4 + 19;
Console.Write(answer);

// prints 23
```