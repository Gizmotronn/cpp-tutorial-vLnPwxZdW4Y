# The If Statement
* The If statement is used to execute code if a condition is true

Syntax for if statement:
```c++
if (condition) {
    statements
}
```

* If the condition is not fulfilled, then the program skips the condition part and goes to the next body
* Relational operators are used to evaluate conditions:

```c++
if (7 > 4) {
  cout <<  "Yes"; 
}

// Outputs "Yes"
```
* If 7 was less than 4, the program would end (since there is nothing after the if statement)
* There are other relational operators: 
    *  >= - greater than or equal to
    *  <= - less than or equal to
    *  == - equal to
    *  != - not equal to

Example of ==:

```c++
if (10 == 10) {
  cout <<  "Yes";
}

// Outputs "Yes"
```

* You can compare variables in the if statement by using relational operators:

```c++
int a = 55;
int b = 33;
if (a > b) {
  cout << "a is greater than b";
}

// Outputs "a is greater than b"
```

# The Else Statement
* An if statement can be followed by an optional else statement
* This statement is executed if the if statement is not fulfilled

An example of an else statement (nothing occurs in this example):

```c++
if (condition) {
  //statements
}
else {
 //statements
}
```

Another example:

```c++
int mark = 90;

if (mark < 50) {
  cout << "You failed." << endl;
}
else {
  cout << "You passed." << endl;
}

// Outputs "You passed."
```

* You can include as many statements as you want:

```c++
int mark = 90;

if (mark < 50) {
  cout << "You failed." << endl;
  cout << "Sorry" << endl;
}
else {
  cout << "Congratulations!" << endl;
  cout << "You passed." << endl;
  cout << "You are awesome!" << endl;
}

/* Outputs
Congratulations!
You passed.
You are awesome!
*/
```

* You can also nest if/else statements

```c++
int mark = 100;

if (mark >= 50) {
  cout << "You passed." << endl;
  if (mark == 100) {
    cout <<"Perfect!" << endl;
  }
}
else {
  cout << "You failed." << endl;
}

/*Outputs
You passed.
Perfect!
*/
```

* C++ provides an unlimited number of nested if/else statements:

```c++
int age = 18;
if (age > 14) {
  if(age >= 18) {
    cout << "Adult";
  }
  else {
    cout << "Teenager";
  }
}
else {
  if (age > 0) {
    cout << "Child";
  }
  else {
    cout << "Something's wrong";
  }
}
```

* A single statement in if/else can be included without enclosing it into curly brackets ({}):

```c++
int a = 10;
if (a > 4)
  cout << "Yes";
else
  cout << "No";
```

# The While Loop
* A loop repeatedly executes a series of statements until a particular condition is satisfied
* A while loop executes a series of statements while a particular condition is __true__

```c++
while (condition) {
   statement(s);
}
```

Another example:

```c++
int num = 1;
while (num < 6) {
  cout << "Number: " << num << endl;
  num = num + 1;
}

/* Outputs
Number: 1
Number: 2
Number: 3
Number: 4
Number: 5 
*/
```
 
* The Increment value can be changed
* If it is, then the number of times something will be done will change as well:

```c++
int num = 1;
while (num < 6) {
  cout << "Number: " << num << endl;
  num = num + 3;
}

/* Outputs
Number: 1
Number: 4 
*/
```

# Using a while loop
* The increment/decrement operators can be used to change the value of a variable in a loop
* A loop can be used to obtain mulitple inputs from the user:

```c++
int num = 1;
int number;

while (num <= 5) {
  cin >> number;
  num++;
}
```

* Let's modify the above code to calculate the sum of the numbers the user has entered:

```c++
int num = 1;
int number;
int total = 0;

while (num <= 5) {
  cin >> number;
  total += number;
  num++;
}
cout << total << endl;
```

* The code above adds the number added by the user to the "total" variable with each "loop iteration"
* Once the loop stops executing, "total" is printed
* Total variable - sum of all numbers user has entered

# The For Loop
* The for loop is a __repetition control structure__
* It allows you to efficiently write a loop that executes __a specific number of times__

Syntax (The For Loop):

```c++
for ( init; condition; increment ) {
  statement(s);
}
```

* The "init" step is executed first. It does not repeat
* The condition is then evaluated. The body of the loop is executed if the condition is true
* The increment statement updates the loop control value

Example:

```c++
for (int x = 1; x < 10; x++) {
 // some code
 }
```

* You can change the increment statement
* You can use a decrement statement

# The Do...While loop
* The do while loop checks its conditions at the bottom of the loop
* Similar to a while loop
    * However, it is guaranteed to execute at least once

Syntax:

```c++
do {
   statement(s);
} while (condition);
```

Example:

```c++
int a = 0;
do {
  cout << a << endl;
  a++;
} while(a < 5);

/* Outputs
0
1
2
3
4
*/
```

* If the condition evaluated to false, the statements in __do__ would still run once
* It executes before evaluating the condition:
```c++
int a = 42;
do {
  cout << a << endl;
  a++;
} while(a < 5);

// Outputs 42
```
* If the condition is never false, the loop will run forever:
```c++
int a = 42;
do {
  cout << a << endl;
} while (a > 0);
```
# The Switch Statement
* Sometimes there is a need to test a value __for equality_ against __multiple values__
* This can be achieved using __multiple if statements__:

```c++
int age = 42;
if (age == 16) {
  cout <<"Too young";
}
if (age == 42) {
  cout << "Adult";
}
if (age == 70) {
  cout << "Senior";
}
```
* The switch statement tests a variable against a list of values...
    * (These variables are called __cases__)
    * ...to determine whether it is equal to any of them
* If a match is found, it executes the statements __in that case__
* The default case can be executed/used to perform a task if none of the cases are determined to be true (this is optional)
```c++
int age = 25;
switch (age) {
  case 16:
    cout << "Too young";
    break;
  case 42:
    cout << "Adult";
    break;
  case 70:
    cout << "Senior";
    break;
  default:
    cout << "This is the default case";
}

// Outputs "This is the default case"
```
* The __break statement__'s role is to __terminate__ the switch statement
* In instances in which the variable is equal to a case, the statements that come after the case continue to execute until they encounter a break statement. In other words, leaving out a break statement results in the execution of all of the statements in the following cases, even those that don't match the expression.

# Logical Operators
* Use logical operators to __combine conditional statements__ and __return true or false__

* && - AND Operator - y && y form
* || - OR Operator - X||y form
* ! - NOT Operator - !x

* AND - both expressions MUST BE TRUE
* OR - any of its "operands" is true
* NOT - if the "operand"/condition/expression is FALSE
AND Operator Example Syntax:
```c++
int age = 20;
if (age > 16 && age < 60) {
  cout << "Accepted!" << endl;
}

// Outputs "Accepted"
```

* Within a single __if statement__, logical operators can be used to __combine multiple conditions__:

```c++
int age = 20;
int grade = 80;

if (age > 16 && age < 60 && grade > 50) {
  cout << "Accepted!" << endl;
}
```

NOT:
```c++
int age = 10;
if ( !(age > 16) ) {
  cout << "Your age is less than 16" << endl;
}

// Outputs "Your age is less than 16"
```

OR:
```c++
int age = 16;
int score = 90;
if (age > 20 || score > 50) {
    cout << "Accepted!" << endl;
}

// Outputs "Accepted!"
```