# Introduction to data types
* The operating system allocates memory
    * It also selects what will be stored in the reserved memory based on the variable's data type
* The data type defines the proper use of an __identifier__, what kind of data can be stored, and which types of operations can be performed
* Data types are intended to define the proper use of an identifier

Example Syntax:
(Legal and illegal C++ expressions):

```c++
55+15 //  legal C++ expression
//Both operands of the + operator are integers

55 + "John" // illegal
// The + operator is not defined for integer and string
```

* Numeric data types include:
    * Integers (whole numbers). Example - -7, 42
    * Floating point numbers. Exampe - 3.14, -14.67

* A string is composed of numbers, characters, or symbols (1, B, ~)
* String literals are placed in __double quotation__ marks
    * Example: "Hello World"
* Characters are single letters of symbols
    * They must be enclosed between single quotes
    * Example: 'a'

* The boolean data type returns just two possible values
    * True (1)
    * False (0)
    * Conditional expressions are an example of the Boolean data type

# Introduction revisited 
#### (also written on paper - soon (will do this for conditionals and loops AND basic concepts)
* The operating system allocates memory 
    * It selects what will be stored in the __reserve memory__ based on the variable's __data type__
* The __data type__ defines the __proper use__ of an *identifier* 
    * It also defines what kind of data can be stored
    * Which type of operations can be performed

__Need to buy note/study stuff__