# Hello, World!

```c++
#include <iostream>
using namespace std;

int main()
{
  cout << "Hello world!";
  return 0;
}
```

The code listed above makes the console print "Hello World".

* {}, indicates beginning and end of a function
    * Many different headers in C++
* At the start of the code, the program calls for the "iostream" header
    * The iostream header defines the standard stream objects that input and output data
* Compilers IGNORE blank lines
    * However, blank lines can improve the readability of the code
* The line "using namespace std" tells the compiler to use the standard namespace
* Program execution begins with the main function - int main()
* Streams are used to perform input and output operations
* In most program environments, the default output destination is the screen
* In C++, "cout" is the stream object used to access it
    * "cout << "Hello World!";" results in Hello World being displayed on the screen
* Semicolon is used to terminate a statement
* A block is a set of logically connected statements
    * They are surrounded by {}
* The last instruction in the program is the return function
    * The line return 0 terminates the main() function

# Printing a text
* You can add multiple "insertion operators (<<)" after "cout"

```c++
cout<<"This"<<"Is"<<"Awesome";
```
* The cout operator does NOT insert a line break at the end of the output
* "endl" manipulator - inserts line break for cout operator

```c++
#include <iostream>
using namespace std;

int main()
{
  cout << "Hello world!" << endl;
  cout << "I love programming!";
  return 0;
}
```

* \n can also be used for a new line/line break

```c++
#include <iostream>
using namespace std;

int main()
{
   cout << "Hello world! \n";
   cout << "I love programming!";
   return 0;
}
```

* Using 2 new lines (\n\n) will create a blank line

# Comments
* The compiler ignores comments
* Comments are used to explain what different parts of the code are for/what they do
* Single line comment - // (two forward slashes)

```c++
#include <iostream>
using namespace std;

int main()
{
   // prints "Hello world"
   cout << "Hello world!";
   return 0;
}
```

* Comments that span multiple lines can be done with /*
For example:

```c++
#include <iostream>
using namespace std;

int main()
{
    /* This comment can 
    span multiple lines
    this part of the code
    prints "Hello world"
    */
    cout << "Hello world!";
    return 0;
}
```

# Variables
* Creating a variable reserves a memory location, or a space in memory for storing values
* The compiler requires you to provide a "data type" for each variable you define/declare
* C++ offers a variety of built-in and user-defined data types
* Integer - built in variable type. Defines a whole number value.
    * Keyword int
* C++ requires that you specify the type and identifier for each variable defined
* Identifier - a name for a variable, function, class, module, or any other user-defined item
    * Starts with a letter (A-Z or a-z) or an underscore (_).
    * It is then followed by additional letters, underscores and digits

Example of integer variable:
```c++
# include <iostream>
using namespace std

int main()
{
    int myVariable = 10
}
```

Now, we can assign a value to a variable and print it:

```c++
# include <iostream>
using namespace std;

int main()
{
    int myVariable = 10;
    cout << myVariable;
    return 0;
}
// outputs 10
/* outputs
10 */
```

* It is possible to define multiple variables in one declaration
    * To do this, separate the variables with commas

```c++
int a, b;
// defines two variables of type int
```

```c++
int num;
cin >> num;
```

* A variable can be assigned a value
    * The variable can then be used to perform operations
    * For example, we can create a variable called "sum" and add 2 variables (integers) together
    * To do this, use the "plus (+) operator"
```c++
int a = 30; 
int b = 15; 
int sum = a + b;
// Now sum equals 45
```

```c++
#include <iostream>
using namespace std;

int main()
{
    int a = 46;
    int b = 1;
    int sum = a+b;
    
    cout << sum;
return 0;
}

// Outputs 47
```
    
}

# Working with variables
* You can assign a value to a variable as soon as you define it or after defining it
* You can also change the value of a variable (just define it again)
* To enable the user to input a value, use "cin" in combination with the "extraction operator (>>)."
    * The variable containing the extracted data follows the operator
    * How to accept user input AND store it in the "num" variable:

```c++
int num;
cin >> num;
```

```c++
#include <iostream>
using namespace std;

int main()
{
   int a;
   cout << "Please enter a number \n";
   cin >> a;

   return 0;
}
```

* You can accept user input multiple times throughout a program:

```c++
#include <iostream>
using namespace std;

int main()
{
    int a, b;
    cout << "Enter a number \n";
    cin >> a;
    cout << "Enter another number \n";
    cin >> b;

    return 0;
}
```

Accepting user input and printing output:

```c++
#include <iostream>
using namespace std;

int main() 
{
  int a, b;
  int sum;
  cout << "Enter a number \n";
  cin >> a;
  cout << "Enter another number \n";
  cin >> b;
  sum = a + b;
  cout << "Sum is: " << sum << endl;

  return 0;
}
```

# More on variables
* You only need to specify the data type for a variable once (unless you change it)
* The variable can then be used without referring to the data type
* The variable's value can be changed at any time without specifying the data type, unless the data type changes

# Basic Arithmetic
C++ supports these mathematic/arithmetic operators:
* Addition (+)
* Subtraction (-)
* Multiplication (*)
* Division (/)
* Modulus (%)

Example of addition:

```c++
int x;
x = 4 + 6;
cout << x << endl;
```

* Remainders are dropped to maintain an integer (true number) value when dividing
* The modulus operator (%)  returns the remainder, but nothing else, from the equation
* BIMDAS - operation precedence

# Assignment Operators
* The "simple assignment operator" (=) assigns the right side to the left side
* You can perform an operation AND a variable assigment at the same time:

```c++
int x = 10;
x += 4; // equivalent to x = x + 4
x -= 5; // equivalent to x = x - 5
```

* The same "shorthand syntax" applies to all other C++ arithmetic operators

```c++
x *= 3; // equivalent to x = x * 3
x /= 2; // equivalent to x = x / 2
x %= 4; // equivalent to x = x % 4
```

* The increment operator is used to increase an integer's value by one:

```c++
x++; //equivalent to x = x + 1
```

```c++
int x = 11;
x++;
cout << x;

// Outputs 12
```

* The increment operator has 2 forms: __prefix__ and __postfix__

```c++
++x; // prefix
x++; // postfix
```

* Prefix increments the value, then proceeds with the expression
* Postfix evaluates the expression then performs the incrementing
* The prefix example increments the value of x, and then assigns it to y.
* The postfix example assigns the value of x to y, and then increments it.
* Decrement operator (--) - opposite of increment operator

```c++
--x; // prefix
x--; // postfix
```

Completed "Basic Concepts"